<?php

namespace Drupal\search_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Returns responses for Search user controller routes.
 */
class SearchUserController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(String $username) {

    $cid = 'username_exists_' . $username;
    $cache = \Drupal::cache()->get($cid);
    $user_exists = FALSE;

    if ($cache === FALSE) {
      $uids = \Drupal::entityQuery('user')
        ->condition('name', $username)
        ->execute();

      if (count($uids)) {
        $user_exists = TRUE;
        $tags = ['user:' . (int) array_values($uids)[0]];
        \Drupal::cache()->set($cid, array_values([TRUE]), CacheBackendInterface::CACHE_PERMANENT, $tags);
      }
    }
    else {
      $user_exists = TRUE;
    }

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $user_exists ? "El usuario existe." : "El usuario no existe.",
    ];

    return $build;
  }

}
