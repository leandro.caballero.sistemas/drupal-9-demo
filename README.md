# Instrucciones para instalar en local

## Prerequisitos
1. Se requiere tener instalado Git.
2. Se requiere tener instalado Docker.
3. Se debe agregar la siguiente línea en el archivo de hosts

```
127.0.0.1   drupalfhios.docker.localhost
```

Dejo un enlace de ayuda para ejecutar esta tarea [Receta ajuste de archivo de hosts para diferentes SO](https://docs.rackspace.com/support/how-to/modify-your-hosts-file/)

## Instrucciones
A continuación el paso a paso a seguir para levantar entorno en local.

1. Se debe clonar el repositorio de
https://gitlab.com/leandro.caballero.sistemas/drupal-9-demo

2. Se debe ingresar a la carpeta recien clonada (drupal-9-demo) y ejecutar 
```
docker composer up -d
```

3. Se debe ingresar al contenedor de PHP ejecutando
```
docker exec -ti my_drupal9_project_php bash
```

 3.a. Ejecutar
 ``` 
 composer install
 ```

 3.b. Ejecutar
 ```
 ./vendor/bin/drush si --existing-config -y
 ```

  Al final de la ejecución aparecen los datos de usuario y password locales, por ejemplo:
  ```
 [success] Installation complete.  User name: admin  User password: AdS4ZriGm2 [83.49 sec, 78.93 MB]
 ```
 
4. Ingresar en el navegador local al siguiente enlace:

http://drupalfhios.docker.localhost:8000/


## TODO
Tema Traducciones, hay partes en español y en inglés, se debería agregar a todos la función t() para la internalizacionación.
